package com.syifana.lesson2_cartShop;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class ShowProductActivity extends AppCompatActivity {
    DBHelper mydb;
    ProductAdapter adapter;
    ArrayList<Product> arrayOfProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_product);

        mydb = new DBHelper(this);
        arrayOfProduct = new ArrayList<Product>();

        this.adapter = new ProductAdapter(this, arrayOfProduct);

        ListView listView = findViewById(R.id.list_view_product);
        listView.setAdapter(adapter);
        showAllData();
    }

    public void showAllData(){
        Cursor cursor = mydb.getAllData();

        while(cursor.moveToNext()){
            Integer cId = cursor.getInt(cursor.getColumnIndexOrThrow(mydb.COLUMN_ID));
            String cName = cursor.getString(cursor.getColumnIndexOrThrow(mydb.COLUMN_PRODUCT_NAME));
            String cDesc = cursor.getString(cursor.getColumnIndexOrThrow(mydb.COLUMN_PRODUCT_DESCRIPTION));
            Integer cImage = cursor.getInt(cursor.getColumnIndexOrThrow(mydb.COLUMN_PRODUCT_IMAGE));
            Integer cPrice = cursor.getInt(cursor.getColumnIndexOrThrow(mydb.COLUMN_PRODUCT_PRICE));
            Integer cChoosen = cursor.getInt(cursor.getColumnIndexOrThrow(mydb.COLUMN_PRODUCT_STATUS));

            adapter.add(new Product(cId, cName, cDesc, cImage, cPrice, cChoosen));
        }
    }
}
