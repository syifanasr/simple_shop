package com.syifana.lesson2_cartShop;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ProductAdapter extends ArrayAdapter<Product> {
    DBHelper mydb;

    public class ViewHolder{
        ImageView imageView;
        TextView tvProductName;
        TextView tvProductPrice;
        Button addCart;
        Button viewDetail;
    }

    public ProductAdapter(Context mContext, ArrayList<Product> mProduct){
        super(mContext, 0, mProduct);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Product product = getItem(position);
        final ViewHolder holder;


        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_detail,parent,false);
            holder = new ViewHolder();

            holder.imageView = convertView.findViewById(R.id.image_view_product);
            holder.tvProductName = convertView.findViewById(R.id.text_view_product_name);
            holder.tvProductPrice = convertView.findViewById(R.id.text_view_product_price);
            holder.addCart = convertView.findViewById(R.id.button_add_cart);
            holder.viewDetail = convertView.findViewById(R.id.button_view_detail);

            if(product.isChoosen == 1){
                Log.d("cek button isChoosen : ", " "+ product.isChoosen);
                holder.addCart.setText("REMOVE");
            }else{
                holder.addCart.setText("ADD TO CART");
            }
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
            Log.d("cek product", "isChoosen : "+ product.isChoosen);
            if(product.isChoosen == 1){
                holder.addCart.setText("REMOVE");
            }else{
                holder.addCart.setText("ADD TO CART");
            }
        }

        Button buttonViewDetail = convertView.findViewById(R.id.button_view_detail);
        final Button buttonAddCArt = convertView.findViewById(R.id.button_add_cart);

        buttonViewDetail.setTag(position);
        buttonAddCArt.setTag(position);

        buttonViewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                Product product = getItem(position);
                Intent intentViewDetail = new Intent(v.getContext(), ShowProductDetailActivity.class);
                intentViewDetail.putExtra("parse_product_name", product.productName);
                intentViewDetail.putExtra("parse_product_price", product.productPrice);
                intentViewDetail.putExtra("parse_product_desc", product.productDesc);
                intentViewDetail.putExtra("parse_product_status", product.isChoosen);
                intentViewDetail.putExtra("parse_product_image", product.imageURL);
                v.getContext().startActivity(intentViewDetail);
            }
        });

        buttonAddCArt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                Product product = getItem(position);
                if(product.isChoosen == 1){
                    product.isChoosen = 0;
                    buttonAddCArt.setText("ADD TO CART");
                }else{
                    product.isChoosen = 1;
                    buttonAddCArt.setText("REMOVE");
                }
                mydb = new DBHelper(v.getContext());
                if(mydb.updateData(product.productID, product.productName, product.productPrice,
                        product.productDesc, product.isChoosen, product.imageURL)){
                    Log.d("Update "+position+" : ", "SUCSESS");
                }else{
                    Log.d("Update "+position+" : ", "FAILED");
                }
            }
        });

        TextView textViewProductName = convertView.findViewById(R.id.text_view_product_name);
        TextView textViewProductPrice = convertView.findViewById(R.id.text_view_product_price);
        ImageView imageProductView = convertView.findViewById(R.id.image_view_product);

        textViewProductName.setText(product.productName);
        textViewProductPrice.setText(product.productPrice +"");
        imageProductView.setImageResource(product.imageURL);

        return convertView;
    }
}
