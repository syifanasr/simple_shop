package com.syifana.lesson2_cartShop;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ShowProductDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_product_detail);

        initGetIntent();
    }

    public void initGetIntent(){
        TextView tvProductNameDetail = findViewById(R.id.text_view_product_name_detail);
        TextView tvProductPriceDetail = findViewById(R.id.text_view_product_price_detail);
        TextView tvProductDetail = findViewById(R.id.text_view_description);
        ImageView ivProductDetail = findViewById(R.id.image_view_detail);

        String parseProductName = "";
        Integer parseProductPrice;
        String parseProductDesc = "";
        Integer parseProductStatus;
        Integer parseImage;

        boolean checkProductName = getIntent().hasExtra("parse_product_name");
        boolean checkProductPrice = getIntent().hasExtra("parse_product_price");
        boolean checkProductDesc = getIntent().hasExtra("parse_product_desc");
        boolean checkProductStatus = getIntent().hasExtra("parse_product_status");
        boolean checkImage = getIntent().hasExtra("parse_product_image");

        if(checkProductName && checkProductPrice && checkProductDesc && checkProductStatus && checkImage){
            parseProductName = getIntent().getExtras().getString("parse_product_name");
            parseProductPrice = getIntent().getExtras().getInt("parse_product_price");
            parseProductDesc = getIntent().getExtras().getString("parse_product_desc");
            parseProductStatus = getIntent().getExtras().getInt("parse_product_status");
            parseImage = getIntent().getExtras().getInt("parse_product_image");

            tvProductNameDetail.setText(parseProductName);
            tvProductPriceDetail.setText(parseProductPrice+"");
            tvProductDetail.setText(parseProductDesc);
            ivProductDetail.setImageResource(parseImage);
        }else{
            tvProductNameDetail.setText("Failed");
            tvProductPriceDetail.setText("Failed");
            tvProductDetail.setText("Failed");
            ivProductDetail.setImageResource(R.drawable.welcome);
        }

    }
}
