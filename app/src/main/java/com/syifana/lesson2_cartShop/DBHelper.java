package com.syifana.lesson2_cartShop;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "cartShopDb";
    public static final String TABLE_NAME = "productlist";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_PRODUCT_NAME = "product_name";
    public static final String COLUMN_PRODUCT_PRICE = "product_price";
    public static final String COLUMN_PRODUCT_DESCRIPTION = "product_description";
    public static final String COLUMN_PRODUCT_STATUS = "product_status";
    public static final String COLUMN_PRODUCT_IMAGE = "product_image";

    private static final String SQL_CREATE_ENTRIES =
            "create table " + TABLE_NAME + " ("+ COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT," +
                    COLUMN_PRODUCT_NAME + " TEXT," + COLUMN_PRODUCT_PRICE + " TEXT,"+
                    COLUMN_PRODUCT_DESCRIPTION + " TEXT,"+ COLUMN_PRODUCT_STATUS + " TEXT,"+ COLUMN_PRODUCT_IMAGE + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public boolean addData(String productName, Integer productPrice, String productDesc,
                           Integer isChoosen, Integer productImage) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_PRODUCT_NAME, productName);
        contentValues.put(COLUMN_PRODUCT_PRICE, productPrice);
        contentValues.put(COLUMN_PRODUCT_DESCRIPTION, productDesc);
        contentValues.put(COLUMN_PRODUCT_STATUS, isChoosen);
        contentValues.put(COLUMN_PRODUCT_IMAGE, productImage);

        long result = db.insert(TABLE_NAME, null, contentValues);
        Log.d("result", (result + ""));
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateData(Integer idProduct, String productName, Integer productPrice, String productDescription,
                              Integer isChoosen, Integer productImage){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_PRODUCT_NAME, productName);
        contentValues.put(COLUMN_PRODUCT_PRICE, productPrice);
        contentValues.put(COLUMN_PRODUCT_DESCRIPTION, productDescription);
        contentValues.put(COLUMN_PRODUCT_STATUS, isChoosen);
        contentValues.put(COLUMN_PRODUCT_IMAGE, productImage);

        String[] tempId = {Integer.toString(idProduct)};
        long result = db.update(TABLE_NAME, contentValues,COLUMN_ID+"= ?", tempId);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                COLUMN_PRODUCT_NAME,
                COLUMN_PRODUCT_PRICE,
                COLUMN_PRODUCT_DESCRIPTION,
                COLUMN_PRODUCT_STATUS,
                COLUMN_PRODUCT_IMAGE
        };
        Cursor cursor = db.query(
                TABLE_NAME,
                projection, null, null,
                null, null, null
        );
        return cursor;
    }

    public Cursor getCart(){
        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                COLUMN_PRODUCT_NAME,
                COLUMN_PRODUCT_PRICE,
                COLUMN_PRODUCT_DESCRIPTION,
                COLUMN_PRODUCT_STATUS,
                COLUMN_PRODUCT_IMAGE
        };

        String selection = COLUMN_PRODUCT_STATUS +" = ?";
        String[] selectionArgs = { Integer.toString(1) };
        Cursor cursor = db.query(
                TABLE_NAME, projection, selection, selectionArgs,
                null, null, null
        );
        return cursor;
    }
}
