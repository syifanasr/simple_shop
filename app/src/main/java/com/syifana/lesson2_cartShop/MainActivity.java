package com.syifana.lesson2_cartShop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public ProductAdapter adapter;

    DBHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mydb = new DBHelper(getApplicationContext());
        initProductData();
    }

    public void initProductData(){
        int[] arrayImage = {
                R.drawable.product1,
                R.drawable.product2,
                R.drawable.product3,
                R.drawable.product4,
                R.drawable.product5,
                R.drawable.product6,
                R.drawable.product7,
                R.drawable.product8,
                R.drawable.product9,
                R.drawable.product10};
        boolean isInsert;
        int i;
        for(i=0; i<10; i++){
            isInsert = mydb.addData("FOCALLURE 18 Colors Eyeshadow Palette Original "+ i,
                    120000+(i*5000)/(i+10),
                    "Highend Eyeshadow", 0, arrayImage[i]);
            if(isInsert){
                Log.d("INSERT",  + i + "SUCSESS" );
            }else{
                Log.d("INSERT",  + i + "FAILED" );
            }
        }
    }

    public void goToShowProductScreen(View view){
        Intent intentShowProduct = new Intent(getApplicationContext(), ShowProductActivity.class);
        startActivity(intentShowProduct);
    }

    public void goToShowCart(View view){
        Intent intentShowCart = new Intent(getApplicationContext(), ShowCartActivity.class);
        startActivity(intentShowCart);
    }


    public void clearItems(){
        this.adapter.clear();
    }
}
