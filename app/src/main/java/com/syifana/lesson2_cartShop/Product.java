package com.syifana.lesson2_cartShop;

public class Product {
    public Integer productID;
    public String productName;
    public String productDesc;
    public Integer imageURL;
    public Integer productPrice;
    public Integer isChoosen = 0;


    public Product(Integer productIDs, String productNames, String productDescs,
                   Integer imageURLs, Integer productPrices, Integer isChoosens){
        this.productID = productIDs;
        this.productName = productNames;
        this.productDesc = productDescs;
        this.imageURL = imageURLs;
        this.productPrice = productPrices;
        this.isChoosen = isChoosens;
    }

    public Integer getProductID() {
        return productID;   }

    public String getProductName() {

        return productName;
    }

    public String getProductDesc() {

        return productDesc;
    }

    public Integer getImageURL() {

        return imageURL;
    }

    public Integer getProductPrice() {
        return productPrice;
    }

    public Integer getIsChoosen() {

        return isChoosen;
    }
}