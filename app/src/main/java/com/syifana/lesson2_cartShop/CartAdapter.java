package com.syifana.lesson2_cartShop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CartAdapter extends ArrayAdapter<Product> {
    int i;

    public class ViewHolder{
        ImageView imageView;
        TextView textViewName;
        TextView textViewPrice;
    }

    public CartAdapter(Context mContext, ArrayList<Product> mProduct){
        super(mContext, 0, mProduct);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Product product = getItem(position);
        final ViewHolder holder;


        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cart_detail,parent,false);
            holder = new ViewHolder();

            holder.imageView = convertView.findViewById(R.id.image_view_product_cart);
            holder.textViewName = convertView.findViewById(R.id.text_view_product_name_cart);
            holder.textViewPrice = convertView.findViewById(R.id.text_view_product_price_cart);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        TextView textViewProductName = convertView.findViewById(R.id.text_view_product_name_cart);
        TextView textViewProductPrice = convertView.findViewById(R.id.text_view_product_price_cart);
        ImageView imageViewProduct = convertView.findViewById(R.id.image_view_product_cart);

        textViewProductName.setText(product.productName);
        textViewProductPrice.setText(product.productPrice +"");
        imageViewProduct.setImageResource(product.imageURL);

        return convertView;
    }
}
